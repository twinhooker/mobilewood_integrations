<?php require_once __DIR__.'/vendor/autoload.php';

use Mobilewood\MerlionSynchronizer;

$synchronizer = new MerlionSynchronizer( 'config.ini' );
$shopCatID    = 154;
$synchronizer->loadCategoryProductIDs( 154 );
$synchronizer->loadCategoryProducts( 154 );
