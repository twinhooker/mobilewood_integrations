create table merlion_category_map( category_id int, merlion_cat_id varchar( 255 ) );

insert into merlion_category_map( category_id, merlion_cat_id )
values ( 154, 'ML4115' ), ( 154, 'ML1611' ), ( 154, 'ML161103' ), ( 154, 'ML0203' );

create table merlion_items_map(
    merlion_id varchar( 255 ) primary key,
    merlion_vendor_part varchar( 255 )
);

create table merlion_not_found_properties (
    name varchar( 255 ) unique,
    value varchar( 255 )
);

alter table phpshop_products add column has_merlion_description boolean default false;
