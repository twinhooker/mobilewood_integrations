<?php

namespace Mobilewood;

use Mobilewood\MerlionClient;
use Mobilewood\PhpShopClient;

class MerlionSynchronizer
{
    private $shopClient;
    private $merlionClient;
    private $propertyMap;
    private $notFoundPropertiesFile;
    private $config;

    public function __construct( $configpath )
    {
        $config = parse_ini_file( $configpath, true );

        $this->config = $config;
        
        $this->shopClient = new PhpShopClient(
            $config['PhpShop']['host'],
            $config['PhpShop']['username'],
            $config['PhpShop']['password'],
            $config['PhpShop']['database']
        );

        $this->merlionClient = new MerlionClient(
            $config['MerlionClient']['username'],
            $config['MerlionClient']['password'],
            $config['MerlionClient']['imageDownloadPath'],
            $config['MerlionClient']['imageURL']
        );

        $this->propertyMap = $this->readPropertyMapCSV( $config['Data']['propertyMap'] );
        $this->notFoundPropertiesFile = $config['Data']['notFoundProperties'];
    }

    public function readPropertyMapCSV( $filepath )
    {
        if ( !file_exists( $filepath ) ) {
            die("File '$filepath' not found.\n");
        }
        $handle = fopen( $filepath, "r" );

        $arr = Array();
        while ( ( $data = fgetcsv( $handle, 0, ";") ) !== FALSE ) {
            $key = iconv( 'CP1251', 'UTF-8', $data[0] );
            $value = iconv( 'CP1251', 'UTF-8', $data[1] );
            $arr[ $key ] = $value;
        }

        fclose( $handle );

        return $arr;
    }

    public function loadCategoryProductIDs( $shopCatID )
    {
        $merlionCatIDs = $this->shopClient->getMerlionCategories( $shopCatID );

        foreach ( $merlionCatIDs as $merlionCatID ) {
            $products = $this->merlionClient->getItems( $merlionCatID );
            $this->shopClient->upsertMerlionItemIDs( $products );
        }

        return;
    }

    public function loadCategoryProducts( $shopCatID )
    {
        $this->shopClient->clearNotFoundProperties();
        $emptyProducts = $this->shopClient->getUpdatableProducts( $shopCatID );

        foreach ( $emptyProducts as $product ) {
            $this->syncProduct( $product );
            $this->shopClient->updateProduct( $product );
            print_r( $product );
            break;
        }
        
        $this->exportNotFoundProperties();
    }

    public function syncProduct( &$product )
    {
        $this->syncProductDescription( $product );
        $this->syncProductImages( $product );
    }

    public function syncProductImages( &$product )
    {
        $merlionProductImages = $this->merlionClient->getItemImages( $product['merlion_id'] );
        $updateRequired = false;

        foreach( $merlionProductImages as $imageType => $imageData )
        {
            if ( $product[$imageType] == '' ) {
                $filePath = $this->merlionClient->downloadImageFile( $imageData );
                $product[$imageType] = $this->config['Data']['imagePrefix'] . $imageData->FileName;
                $updateRequired = true;
            }
        }
    }

    public function syncProductDescription( &$product )
    {
        $merlionItemProperties = $this->merlionClient->getItemProperties( $product['merlion_id'] );

        $vendor_arr = unserialize( $product['vendor_array'] );
        if ( !isset( $vendor_arr ) ) {
            $vendor_arr = Array();
        }

        foreach ( $merlionItemProperties as $merlionProperty ) {
            $phpShopProperty = $this->getShopPropertyFor( $merlionProperty );
            # If property not found leave it in not found sections
            if ( ! isset( $phpShopProperty ) ) {
                $this->shopClient->upsertNotFoundProperty( $merlionProperty );
                print( "Skiping property $merlionProperty->PropertyName\n" );
                continue;
            }

            $phpShopPropertyValue = $this->getShopPropertyValue( $phpShopProperty, $merlionProperty->Value );
            if ( !isset( $phpShopPropertyValue ) ) {
                continue;
            }
            $vendor_arr[ $phpShopProperty['id'] ] = $phpShopPropertyValue['id'];
        }

        if ( count( $vendor_arr ) ) {
            $product['vendor_array'] = serialize( $vendor_arr );
        }
    }

    public function getShopPropertyValue( $phpShopProperty, $value )
    {
        $propertyValue = $this->shopClient->getPropertyValue( $phpShopProperty['id'], $value );

        if ( !isset( $propertyValue ) ) {
            $propertyValue = $this->shopClient->createPropertyValue( $phpShopProperty['id'], $value );
        }

        return $propertyValue;
    }

    public function getShopPropertyFor( $merlionProperty )
    {
        $propertyName = $merlionProperty->PropertyName;
        if ( array_key_exists( $propertyName, $this->propertyMap ) ) {
            $propertyName = $this->propertyMap[ $propertyName ];
        }
        
        $property = $this->shopClient->getItemPropertyByName( $propertyName );

        return $property;
    }


    public function exportNotFoundProperties()
    {
        $properties = $this->shopClient->getNotFoundProperties();
        $handle = fopen( $this->notFoundPropertiesFile, "w" );

        fputs( $handle, $bom = ( chr(0xEF) . chr(0xBB) . chr(0xBF) ) );
        foreach ( $properties as $property ) {
            fputcsv( $handle, $property, ";" );
        }
        fclose( $handle );
    }
}
