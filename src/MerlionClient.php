<?php

declare(strict_types=1);

namespace Mobilewood;

// require 'vendor/autoload.php';

use \SoapClient;

/**
 * Class Client
 */

class MerlionClient
{
    private $client;
    private $wdsl_url = 'https://apitest.merlion.com/re/mlservice3?wsdl';
    private $username;
    private $password;
    private $encoding = 'utf8';
    private $last_request_time;
    private $imageDownloadPath;
    private $imageURL;
    
    public function __construct( $username, $password, $imageDownloadPath, $imageURL )
    {
        
        if ( !isset( $username ) || !isset( $password ) ) {
            throw 'Username and password must be set.';
        }

        $this->username = $username;
        $this->password = $password;
        $this->imageDownloadPath = $imageDownloadPath;
        $this->imageURL = $imageURL;

        $params = array(
            'login'    => $this->username,
            'password' => $this->password,
            'encoding' => $this->encoding,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
            'trace'    => 1,
        );
        $this->client = new SoapClient( $this->wdsl_url, $params );
    }
   
    private function client() {
        $current_time = time();
        if ( $current_time - $this->last_request_time < 2 ) {
            sleep( 1 );
        }
        $this->last_request_time = $current_time;

        return $this->client;
    }

    public function getItems( $catID ) {
        $items = array();
        $i = 0;
        $items_part = $this->client()->getItems( $catID, '', NULL, $i, 1000 );

        while ( count(  $items_part->item ) >= 1000 ) {
            $items = array_merge( $items, $items_part->item );
            $i++;
            $items_part = $this->client()->getItems( $catID, '', NULL, $i, 1000 );
        }
        $items = array_merge( $items, $items_part->item );

        return $items;
    }

    public function getItemImages( $id )
    {
        $result = $this->client()->getItemsImages( '', Array( $id ) );
        $result_image = Array();
        
        foreach ( $result->item as $image ) {
            if (    $image->ViewType == 'v'
                 && $image->SizeType == 'b'
            ) {
                $result_image['pic_big'] = $image;
            }
            elseif (    $image->ViewType == 'v'
                     && $image->SizeType == 's'
            ) {
                $result_image['pic_small'] = $image;
            }
        }

        return $result_image;
    }

    public function downloadImageFile( $imageData )
    {
        $url = $this->imageURL . $imageData->FileName;
        $filePath = $this->imageDownloadPath . $imageData->FileName;

        $fh = fopen( $filePath, 'w+' );
        if (!$fh ) {
            die( 'File open Error: ' . $filePath );
        }

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_FILE, $fh );
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 1000 );
        curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/5.0' );
        curl_exec( $ch );

        curl_close( $ch );
        fclose( $fh );

        return $filePath;
    }
    
    public function getItemPropertySection( $id = NULL ) {
        $result = $this->client()->getItemsPropertiesSections( $id );
        return $result->item[0];
    }

    public function getItemProperties( $itemID ) {
        $itemPropertiesResponse = $this->client()->getItemsProperties( '', Array( $itemID ) );
        return $itemPropertiesResponse->item;
    }

    public function getCatalogs() {
        $catalogs = $this->client()->getCatalog( 'All' );
        foreach ( $catalogs->item as $catalog ) {
            $this->log( var_export( $catalog, true ) );
        }
    }
    
    public function orderItem( $merlionItemID, $qty, $shipmentCode, $date = NULL ) {
        $orderID      = $this->getOrderIDTo( $shipmentCode, $date );
        $operation_no = $this->addItemsToOrder( $merlionItemID, $qty, $orderID );

        $results = $this->client()->getCommandResult( $operation_no );
        while ( $results->item[0]->ProcessingResult == 'Активно' ) {
            $results = $this->client()->getCommandResult( $operation_no );
            $this->log( var_export( $results->item[0], true ) );
        }
    }
    
    public function getOrderIDTo( $where, $current_date ) {
        if ( !isset( $current_date ) ) {
            $current_date = date( 'Y-m-d' );
        }
        $orderList = $this->client()->getOrdersList();

        foreach ( $orderList->item as $order ) {
            $this->log( var_export( $order, true ) );
            if (    $order->ShipmentMethodCode == $where
                 && $order->ShipmentDate       >= $current_date
                 && $order->Status             == '-'
            ) {
                $order_no = $order->document_no;
                break;
            }
        }

        if ( !isset( $order_no ) ) {
            $order_no = $this->createOrder( $where );
        }

        return $order_no;
    }

    public function createOrder( $where ) {
        $shipmentMethod = $this->getShipmentMethod( $where );
        $shipmentDates  = $this->getShipmentDates();
        $shipmentDate   = min( $shipmentDates );
        $counterAgent   = $this->getCounterAgent();

        $orderHeaderRequest = new OrderHeaderRequest();
        $orderHeaderRequest->shipment_method = $shipmentMethod->Code;
        $orderHeaderRequest->shipment_date   = $shipmentDate->Date;
        $orderHeaderRequest->counter_agent   = $counterAgent->Code;
        $operation_no                        = $setOrderHeaderResponse->setOrderHeaderCommandResult;

        $this->waitCommand( $operation_no );
    }

    private function waitCommand ( $operation_no ) {
        $operationResult = $this->client()->getCommandResult( $operation_no );
        $this->log( var_export( $operationResult, true ) );
    }

    public function getMerlionCategories ( ) {
        $categories = $this->client()->getCatalog( 'All' );
        $this->log( var_export( $categories, true ) ); 
    }
    
    private function getShipmentMethod( $where ) {
        $shipmentMethodsResponse = $this->client()->getShipmentMethods();
        foreach ( $shipmentMethodsResponse->item as $item ) {
            if ( $item->Code == $where ) {
                    $shipmentMethod = $item;
                    break;
                }
        }
        if ( !isset( $shipmentMethod ) ) {
            die( "No shipmentMethod found" );
        }

        return $shipmentMethod;
    }

    private function addItemsToOrder( $merlionItemID, $qty, $orderID ) {
        $orderLines = $this->client()->getOrderLines( $orderID );

        foreach( $orderLines->item as $orderLine ) {
            if ( $merlionItemID == $orderLine->item_no ) {
                $qty += $orderLine->qty;
                break;
            }
        }

        $operation_no = $this->client()->setOrderLineCommand( $orderID, $merlionItemID, $qty );

        return $operation_no;
    }


    // LOG

    private function log ( $str = '' ) { 
        file_put_contents( "/tmp/merlion_test", $str . "\n", FILE_APPEND );
    }

}

