<?php

declare(strict_types=1);

namespace Mobilewood;

use \mysqli;

class PhpShopClient
{
    private $dbh;

    public function __construct( $host, $db_name, $username, $password )
    {            
        if (    !isset( $username )
             || !isset( $password )
             || !isset( $host )
             || !isset( $db_name )
        ) {
            throw 'Host, db_name, username and password must be set.';
        }

        $this->dbh = new mysqli( $host, $db_name, $username, $password );
        if ( $this->dbh->errno ) {
            die( "Error: " . $this->dbh->connect_error );
        }
    }

    public function getMerlionCategories( $shopCatID )
    {
        $stmt = $this->dbh->prepare( "select merlion_cat_id from merlion_category_map where category_id = ?" );
        $stmt->bind_param( "i", $shopCatID );
        $stmt->execute();

        $result = $stmt->get_result();
        while ( $row = $result->fetch_assoc() ) {
            $categories[] = $row['merlion_cat_id'];
        }
        return $categories;
    }

    public function updateProduct ( $product )
    {
        $stmt = $this->dbh->prepare(
            "update phpshop_products
             set vendor_array = ?, 
                 pic_big = ?,
                 pic_small = ?,
                 has_merlion_description = true
             where id = ?" );
        $stmt->bind_param(
            "sssi",
            $product['vendor_array'],
            $product['pic_big'],
            $product['pic_small'],
            $product['id'] );
        
        if ( !$stmt->execute() ) {
            die( "Error: " . $stmt->error );
        }

        return;
    }

    public function getUpdatableProducts( $shopCatID )
    {
        $stmt = $this->dbh->prepare(
            "select * from phpshop_products 
             join merlion_items_map on phpshop_products.uid = merlion_items_map.merlion_vendor_part 
             where category = ? and (vendor_array = '' OR has_merlion_description = true )"
        );
        $stmt->bind_param( "i", $shopCatID );
        $stmt->execute();

        $result = $stmt->get_result();
        $products = $result->fetch_all( MYSQLI_ASSOC );

        return $products;
    }

    public function createPropertyValue( $propertyID, $propertyValue )
    {
        $this->log( "CREATING PROPERTY VALUE $propertyID $propertyValue \n" );
        $stmt = $this->dbh->prepare( "insert into phpshop_sort( name, category, pic_url ) values ( ?, ?, '' ) " );
        $stmt->bind_param( "si", $propertyValue, $propertyID );

        if ( !$stmt->execute() ) {
            print_r( sprintf( "Ошибка: %s\n", $stmt->error ) );
            die();
            $this->log( sprintf( "Ошибка: %s\n", $stmt->error ) );
        }

        return $this->getPropertyValue( $propertyID, $propertyValue );
    }

    public function getPropertyValue ( $propertyID, $propertyValue )
    {
        $stmt = $this->dbh->prepare( "select * from phpshop_sort where category = ? and name = ?"  );
        $stmt->bind_param( "is", $propertyID, $propertyValue );
        $stmt->execute();

        $result = $stmt->get_result();
        $propertyValue = $result->fetch_assoc();

        return $propertyValue;
    }


    public function getItemPropertyByName( $name )
    {
        $stmt = $this->dbh->prepare( "select * from phpshop_sort_categories where name = ? and category != 0" );
        $stmt->bind_param( "s", $name );
        $stmt->execute();

        $result  = $stmt->get_result();
        $property = $result->fetch_assoc();

        return $property;
    }

    public function upsertNotFoundProperty( $merlionProperty )
    {
        $stmt = $this->dbh->prepare( "insert ignore into merlion_not_found_properties ( name, value ) values ( ?, ? )" );
        $stmt->bind_param( "ss", $merlionProperty->PropertyName, $merlionProperty->Value );
        $stmt->execute();

        return;
    }

    public function clearNotFoundProperties()
    {
        $this->dbh->query( "delete from merlion_not_found_properties" );

        return;
    }

    public function getNotFoundProperties()
    {
        $result = $this->dbh->query( "select * from merlion_not_found_properties" );
        $properties = $result->fetch_all();

        return $properties;
    }
    public function getItemDescriptions ( $item ) {
        $result = $this->dbh->query( "select * from phpshop_sort_categories where category = 0" );

        while ( $row = $result->fetch_assoc() ) {
            $sort_arr[ $row['id'] ]['name'] = $row['name'];

            $result2 = $this->dbh->query( "select * from phpshop_sort_categories where category = ".$row['id']."  order by num asc" );

            $ii = 0;
            while ( $row2 = $result2->fetch_assoc() ) {
                $sort_arr[ $row['id'] ]['val'][$ii] = $row2['id'];
                $sort_arr[ $row['id'] ]['val_name'][$ii] = $row2['name'];
                $ii++;
            }
        }

        return $sort_arr;
    }
    
    public function upsertMerlionItemIDs ( $items ) {
        $query = "insert ignore into merlion_items_map ( merlion_id, merlion_vendor_part ) values ";

        foreach ( $items as $item ) {
            $merlionID         = $this->dbh->escape_string( $item->No );
            $merlionVendorPart = $this->dbh->escape_string( $item->Vendor_part );
            $query .= "( '$merlionID', '$merlionVendorPart' ),";
        }

        $query = rtrim( $query, ',' );
        if ( !$this->dbh->query( $query ) ) {
            $this->log( sprintf( "Ошибка: %s\n", $this->dbh->error ));
        }
    }
    
    private function log ( $str = '' ) { 
        file_put_contents( "/tmp/merlion_test", $str . "\n", FILE_APPEND );
    }
}
